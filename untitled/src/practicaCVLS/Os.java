package practicaCVLS;

public class Os extends Animal{
    int energia = 20;
    public String toString() {
        if (hibernacio)
            return "\ud83d\udc3c";
        else
            return "\ud83d\udc3b";
    } // icona de os
    @Override
    public void mou(){
        Bloc[][] tauler = ConillsVsLlopsSim.tauler;
        if (hibernacio) return;
        if(energia == 0){
            hibernacio();
            System.out.println("Un oso ha entrado en hibernacion");
        }
        else {
            int i = (int) (Math.random() * 3) - 1 + x;
            int j = (int) (Math.random() * 3) - 1 + y;
            boolean dinsTauler = i >= 0 && i < tauler.length && j >= 0 && j < tauler[0].length;
            if (dinsTauler && !tauler[i][j].esConill()) {
                if (tauler[i][j].esLlop()) {
                    tauler[i][j].getAnimal().mor();
                    energia += 10;
                }
                tauler[x][y].delAnimal();
                tauler[i][j].setAnimal(this);
                x = i;
                y = j;
                energia --;
            }
        }
    }
    public Os(int x, int y) {
        super(x, y);
    }
}

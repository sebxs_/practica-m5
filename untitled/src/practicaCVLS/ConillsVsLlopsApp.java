package practicaCVLS;

import java.util.ArrayList;
import java.util.Scanner;

import static practicaCVLS.ConillsVsLlopsSim.animals;
import static practicaCVLS.ConillsVsLlopsSim.totalHibernate;

public class ConillsVsLlopsApp {
    public static void main(String[] args) {
        ConillsVsLlopsSim joc = new ConillsVsLlopsSim();
        joc.inicialitza();
        Scanner scanner = new Scanner(System.in);
            while (animals.size() > 0) {
                ArrayList<Animal> morts = new ArrayList<>();
                ArrayList<Animal> hibernates = new ArrayList<>();
                for (Animal animal : animals) {
                    animal.mou();
                }
                for (Animal animal : animals) {
                    if (animal.mort) morts.add(animal);
                }
                for (Animal animal : animals) {
                    if (animal.hibernacio) {
                        hibernates.add(animal);
                        totalHibernate++;
                    }
                }
                for (Animal animal : morts) {
                    animals.remove(animal);
                }
                for (Animal animal : hibernates){
                    animals.remove(animal);
                }
                joc.torns++;
                joc.mostra();
                scanner.nextLine();
            }
        joc.mostra();
        System.out.println("Has destruit tota vida a la vista, bon treball \uD83D\uDE05");
    }
}